﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods 
{
    public static void Fill(this Cell[,] grid, GameObject prefab, TileThemeObject theme)
    {

        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                grid[i, j] = new Cell(prefab, theme, i, j);
    }
    public static void Update(this Cell[,] grid, TileThemeObject theme, float fillrate = 0.5f)
    {
        for (int i = 0; i < Utils.Columns; i++)
            for (int j = 0; j < Utils.Rows; j++)
                grid[i, j].UpdateTileTheme(theme, fillrate);
    }
}
