﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Cell
{
    readonly GameObject Prefab;
    public GameObject gameObject;
    public SpriteRenderer renderer;
    public int X;
    public int Y;
    public TileThemeObject Theme;
    public CellDecoration Decoration;
    public Cell(GameObject prefab, TileThemeObject theme, int x, int y)
    {
        Prefab = prefab;
        X = x;
        Y = y;
        gameObject = GameObject.Instantiate(prefab, Utils.GridToWorldPosition(x, y), Quaternion.identity);
        renderer = gameObject.GetComponent<SpriteRenderer>();
        gameObject.name = "X: " + x + "Y: " + y;
        Theme = theme;
        renderer.sprite = theme.tiles[Utils.GetTile(X, Y)];
        Decoration = new CellDecoration();

        //if (!Utils.IsEdgeTile(x, y))
       // {
            if (Random.Range(0.0f, 1.0f) > 0.5f)
                AddDecoration();
       // }
    }

    public void UpdateTileTheme(TileThemeObject theme, float fillRate = 0.5f)
    {
        Theme = theme;
        renderer.sprite = theme.tiles[Utils.GetTile(X, Y)];

        ClearDecoration();
        if (!Utils.IsEdgeTile(X, Y) && Random.Range(0.0f, 1.0f) > fillRate)
            AddDecoration();

    }
    public void AddDecoration()
    {
        if (!Decoration.isCreated)
            Decoration = new CellDecoration(Prefab, Theme.decorations[Random.Range(0, Theme.decorations.Length)], X, Y, gameObject.transform);
    }
    public void ClearDecoration()
    {
        GameObject.Destroy(Decoration.gameObject);
        Decoration = new CellDecoration();
    }
}


public struct CellDecoration
{
    public bool isCreated;
    public GameObject gameObject;
    public SpriteRenderer renderer;
    public TileDecorationObject data;
    public CellDecoration(GameObject prefab, TileDecorationObject decoration, int x, int y, Transform parent = null)
    {
        data = decoration;
        gameObject = GameObject.Instantiate(prefab, Utils.GridToWorldPosition(x, y), Quaternion.identity, parent);
        gameObject.name = "X: " + x + "Y: " + y;
        renderer = gameObject.GetComponent<SpriteRenderer>();
        renderer.sprite = decoration.sprite;
        renderer.sortingOrder = 1;
        isCreated = true;
    }
    public void UpdateTile(TileThemeObject theme)
    {
        if (renderer)
            renderer.sprite = theme.decorations[Random.Range(0, theme.decorations.Length)].sprite;
    }
}