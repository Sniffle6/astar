﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Decoration", menuName = "Tiles/Decoration")]
public class TileDecorationObject : ScriptableObject
{
    public Sprite sprite;
    public int height;
}
