﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public GameObject tilePrefab;
    public TileThemeObject[] themes;
    public int currentTheme;
    public Cell[,] Grid;
    Astar astar;
    Coroutine createPath;
    // Start is called before the first frame update
    void Start()
    {
        SetUpGrid();
        astar = new Astar(Grid);

    }
    public void SetUpGrid()
    {
        Utils.Vertical = (int)Camera.main.orthographicSize;
        Utils.Horizontal = Utils.Vertical * (Screen.width / Screen.height);
        Utils.Columns = Utils.Horizontal * 2;
        Utils.Rows = Utils.Vertical * 2;
        Grid = new Cell[Utils.Columns, Utils.Rows];
        Grid.Fill(tilePrefab, themes[currentTheme]);
    }

    public void UpdateTileTheme(float fillRate)
    {
        currentTheme = currentTheme < themes.Length - 1 ? currentTheme += 1 : 0;
        Grid.Update(themes[currentTheme], fillRate);
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            UpdateTileTheme(0.25f);
        if (Input.GetKeyDown(KeyCode.W))
            UpdateTileTheme(0.5f);
        if (Input.GetKeyDown(KeyCode.E))
            UpdateTileTheme(0.9f);

        if (Input.GetMouseButtonDown(0))
        {
            if (createPath != null)
                StopCoroutine(createPath);
            createPath = StartCoroutine(astar.CreatePath(Grid));
        }

        for (int i = 0; i < astar.closedSet.Count; i++)
            Grid[astar.closedSet[i].X, astar.closedSet[i].Y].renderer.color = Color.red;
        for (int i = 0; i < astar.openSet.Count; i++)
            Grid[astar.openSet[i].X, astar.openSet[i].Y].renderer.color = Color.green;
        for (int i = 0; i < astar.path.Count; i++)
            Grid[astar.path[i].X, astar.path[i].Y].renderer.color = Color.blue;
        if (astar.start != null)
            Grid[astar.start.X, astar.start.Y].renderer.color = Color.cyan;
        if (astar.end != null)
            Grid[astar.end.X, astar.end.Y].renderer.color = Color.black;


    }


}
