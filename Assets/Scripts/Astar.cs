﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astar
{
    public Spot[,] Spots;
    public List<Spot> openSet = new List<Spot>();
    public List<Spot> closedSet = new List<Spot>();
    public List<Spot> path = new List<Spot>();
    public Spot start;
    public Spot end;
    public Astar(Cell[,] grid)
    {
        Spots = new Spot[Utils.Columns, Utils.Rows];
    }

    public IEnumerator CreatePath(Cell[,] grid)
    {
        for (int i = 0; i < Utils.Columns; i++)
        {
            for (int j = 0; j < Utils.Rows; j++)
            {
                Spots[i, j] = new Spot(i ,j, grid[i,j].Decoration.isCreated ? grid[i, j].Decoration.data.height : 0);
            }
        }
        start = Spots[0, 0];
        end = Spots[Utils.Columns - 1, Utils.Rows - 1];
        openSet.Add(start);



        while (openSet.Count > 0)
        {
            int winner = 0;
            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet[i].F < openSet[winner].F)
                    winner = i;
                else if (openSet[i].F == openSet[winner].F)
                    if (openSet[i].H < openSet[winner].H)
                        winner = i;
            }
            Spot current = openSet[winner];

            path = new List<Spot>();
            var temp = current;
            path.Add(temp);
            while(temp.previous != null)
            {
                path.Add(temp.previous);
                temp = temp.previous;
            }


            if (current == end)
                break;

            openSet.Remove(current);
            closedSet.Add(current);

            if (current.Neighboors.Count == 0)
                current.AddNeighboors(Spots);

            var neighboors = current.Neighboors;
            for (int i = 0; i < neighboors.Count; i++)
            {
                var n = neighboors[i];
                if (!closedSet.Contains(n) && n.Height < 1)
                {
                    int tempG = current.G + 1;
                    bool newPath = false;
                    if (openSet.Contains(n))
                    {
                        if(tempG < n.G)
                        {
                            n.G = tempG;
                            newPath = true;
                        }
                    }
                    else
                    {
                        n.G = tempG;
                        newPath = true;
                        openSet.Add(n);
                    }
                    if (newPath)
                    {
                        n.H = Heuristic(n, end);
                        n.F = n.G + n.H;
                        n.previous = current;
                    }
                }
            }

        }
        yield return new WaitForEndOfFrame();
    }
    private int Heuristic(Spot a, Spot b)
    {
        var dx = System.Math.Abs(a.X - b.X);
        var dy = System.Math.Abs(a.Y - b.Y);
        return 1 * ( dx + dy);
    }
}


public class Spot
{
    public int X;
    public int Y;
    public int F;
    public int G;
    public int H;
    public int Height;
    public List<Spot> Neighboors = new List<Spot>();
    public Spot previous;
    public Spot(int x, int y, int height)
    {
        X = x;
        Y = y;
        F = 0;
        G = 0;
        H = 0;
        Height = height;
    }
    public void AddNeighboors(Spot[,] grid)
    {
        if (X < Utils.Columns - 1)
            Neighboors.Add(grid[X + 1, Y]);
        if (X > 0)
            Neighboors.Add(grid[X - 1, Y]);
        if (Y < Utils.Rows - 1)
            Neighboors.Add(grid[X, Y + 1]);
        if (Y > 0)
            Neighboors.Add(grid[X, Y - 1]);
    }
}